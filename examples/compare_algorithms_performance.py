#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Script comparing the performance (execution time) of different deconvolution
methods

"""

import sys
import timeit

import numpy as np

import beamlib

if __name__ == '__main__':
    
    # Defining the microphone array
    # Underbrink array with 65 microphones
    array = beamlib.arrays.Underbrink(z=1.2)
    
    # Defining the the scanning grid
    # Uniform square mesh, from -0.5 to 0.5 with 21 points on each side
    grid = beamlib.grid.Uniform(
        n=21,x=[-0.5,0.5],y=[-0.5,0.5],z=0)
    
    # Random source distribution
    sources = [
        grid.coordinates[ind,:] 
            for ind in np.random.randint(0, high=grid.N, size=10)
        ]
    # incoherent sources of constant amplitude
    sound = beamlib.signal.IncoherentMonopole(
        array,sources,amplitudes=1.0,
        )
    
    # beamforming frequency, Hz
    frequency = 2000
    
    # calculate cross-spectral matrix (CSM)
    sound.calc_CSM(nfft=512,frequency=frequency)
    
    methods = [
        beamlib.beamforming.DAMAS(array=array,grid=grid,frequency=frequency),
        beamlib.beamforming.CLEAN_SC(array=array,grid=grid,frequency=frequency)
    ]
    
    solve_args = [
        dict(algo='NNLS',remove_auto=False),
        dict(relax=0.9,convergence=1e-10),
        ]
    
    number_runs = 10
    
    for method, solve_arg in zip(methods,solve_args):
        
        elapsed_time = 0
        
        for i in range(number_runs):
            print(i)
            start = timeit.default_timer()
            # performing the beamforming calculation
            beamforming_map = method.solve(
                sound.CSM,verbose=False,**solve_arg)
            end = timeit.default_timer()
            elapsed_time += end - start
        
        print(f'Average time: {elapsed_time/number_runs} ms')
    
