#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Script comparing different algorithms for solving the DAMAS deconvolution
method. Consider a synthetic signal with non-coherent monopoles as sources.

"""

import sys

import numpy as np
import matplotlib.pyplot as plt

import beamlib

if __name__ == '__main__':
    
    # Defining the microphone array
    # Arcondoulis array with 64 microphones, placed at 1.2 meters from
    # the focal plan
    array = beamlib.arrays.Arcondoulis(
        N=64, phi=15*np.pi/2, ex=0.9, ey=0.9,
        r0=0.075,rmax=0.5,
        array_center=[0,0],z=0.0,
        )
    
    # Defining the the scanning grid
    # Uniform square mesh, from -0.5 to 0.5 with 15 points on each side
    grid = beamlib.grid.Uniform(
        n=15,x=[-0.5,0.5],y=[-0.5,0.5],z=1.2)
    
    # reproducing the source distribution as in Figure 12 of
    # Xu et al. 2021
    sources = [[  grid.x[0], grid.y[-1],  1.2],
               [  grid.x[4],  grid.y[0],  1.2],
               [  grid.x[7],  grid.y[0],  1.2],
               [  grid.x[9],  grid.y[3],  1.2],
               [ grid.x[-2],  grid.y[2],  1.2],
               [ grid.x[-1],  grid.y[1],  1.2]]
    
    # incoherent sources of constant amplitude
    sound = beamlib.signal.IncoherentMonopole(
        array,sources,amplitudes=1.0,
        )
    
    # calculate cross-spectral matrix (CSM)
    sound.calc_CSM(nfft=512,frequency=2000)
    
    method = beamlib.beamforming.DAMAS(array=array,grid=grid,frequency=2000)
    
    solve_args = [
        dict(algo='Gauss',relax=0.9,it_max=100,remove_auto=True),
        dict(algo='Gauss',relax=0.9,it_max=100,remove_auto=False),
        dict(algo='NNLS',remove_auto=True),
        dict(algo='NNLS',remove_auto=False),
        ]
    
    fig, axs = plt.subplots(
        1,len(solve_args),
        figsize=(len(solve_args)*9/2.54,6/2.54),
        constrained_layout=True
        )
    
    for solve_arg, ax in zip(solve_args,axs):
        # performing the beamforming calculation
        beamforming_map = method.solve(
            sound.CSM,verbose=True,**solve_arg)
    
        _, _, mappable = beamlib.visualize.surface.plot_grid(
            grid.x,grid.y,method.level, ax=ax)
        
        cbar = beamlib.visualize.surface.add_colorbar(
            fig,mappable,label=r'$\Delta$ source power, dB',
            ax=ax,fraction=0.5)
        
        # plotting the array of microphones and the sources positions
        array.plot(ax=ax)
        sound.plot(ax=ax)
        # defining the colomap/colorbar limits
        mappable.set_clim([-15,0])
        
        title = solve_arg['algo']
        if not solve_arg['remove_auto']: title += ' (with diagonal)'
        ax.set_title(title, size=7)
    
