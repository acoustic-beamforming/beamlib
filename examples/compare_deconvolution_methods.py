#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Script comparing different deconvolution methods. Consider a synthetic signal
with non-coherent monopoles as sources.

"""

import sys

import numpy as np

import beamlib

if __name__ == '__main__':
    
    # Defining the microphone array
    # Arcondoulis array with 64 microphones, placed at 1.2 meters from
    # the focal plan
    array = beamlib.arrays.Arcondoulis(
        N=64, phi=11*np.pi/2, ex=0.9, ey=0.9,
        r0=0.1,rmax=0.5,
        array_center=[0,0],z=0.0,
        )
    
    # Defining the the scanning grid
    # Uniform square mesh, from -0.5 to 0.5 with 15 points on each side
    grid = beamlib.grid.Uniform(
        n=15,x=[-0.5,0.5],y=[-0.5,0.5],z=1.2)
    
    # reproducing the source distribution as in Figure 12 of
    # Xu et al. 2021
    sources = [[  grid.x[0], grid.y[-1],  1.2],
               [  grid.x[4],  grid.y[0],  1.2],
               [  grid.x[7],  grid.y[0],  1.2],
               [  grid.x[9],  grid.y[3],  1.2],
               [ grid.x[-2],  grid.y[2],  1.2],
               [ grid.x[-1],  grid.y[1],  1.2]]
    
    # sound = beamlib.signal.TonalMonopole(
    #     array,sources,frequencies=2000,amplitudes=1.0,
    #     )
    # incoherent sources of constant amplitude
    sound = beamlib.signal.IncoherentMonopole(
        array,sources,amplitudes=1.0,
        )
    
    # calculate cross-spectral matrix (CSM)
    sound.calc_CSM(nfft=512,frequency=2000)
    
    method_args = dict(array=array,grid=grid,frequency=2000)
    
    # loop for several deconvolution techniques
    for method in [
            beamlib.beamforming.ConventionalBF(**method_args),
            beamlib.beamforming.DAMAS(**method_args),
            beamlib.beamforming.CLEAN_PSF(**method_args),
            beamlib.beamforming.CLEAN_SC(**method_args),
            ]:
        # performing the beamforming calculation
        beamforming_map = method.solve(sound.CSM)
    
        # plotting the result
        fig, ax, mappable = beamlib.visualize.surface.plot_grid(
            grid.x,grid.y,method.level)
        cbar = beamlib.visualize.surface.add_colorbar(
            fig, mappable, label=r'$\Delta$ source power, dB')
        
        # plotting the array of microphones and the sources positions
        array.plot()
        sound.plot()
        # defining the colomap/colorbar limits
        mappable.set_clim([-15,0])
        
        ax.set_title(method.name, size=7)
    
