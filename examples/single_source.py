#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Example of how to use the library to calculate a beamforming map for
a synthetic signal with coherent monopoles as sources

"""

import sys

import numpy as np

import beamlib

if __name__ == '__main__':
    
    # Defining the microphone array
    # Underbrink with 65 microphones, placed at 0.0 meters from
    # the focal plan
    array = beamlib.arrays.Underbrink(
        Na=8, Nm=8, v=5*np.pi/16, r0=0.1, rmax=0.5,
        array_center=[0,0], z=0.0, center_mic=True,
        )
    
    # Defining the the scanning grid
    # Uniform square mesh, from -0.5 to 0.5 with 15 points on each side
    grid = beamlib.grid.Uniform(
        n=15,x=[-0.5,0.5],y=[-0.5,0.5],z=1.2)
    
    # Creating synthetic sound for a source at [0,0,1.2]
    
    # sinusoidal source
    sound = beamlib.signal.TonalMonopole(
        array,
        [[0.0,0.0,1.2]],
        frequencies=2000,
        )
    # calculate cross-spectral matrix (CSM)
    sound.calc_CSM()
    
    # incoherent source
    sound = beamlib.signal.IncoherentMonopole(
        array,
        [[0.0,0.0,1.2]],
        )
    # calculate cross-spectral matrix (CSM)
    sound.calc_CSM(nfft=512,frequency=2000)
    
    
    # Selecting the beamforming technique to be used
    # the necessary vectors (such as the steering vetors) are calculated on
    # instancing, so its is not necessary to recalculate when considering
    # different signals associated with the same microphone array and
    # scan points
    
    # Conventional beamforming
    method = beamlib.beamforming.ConventionalBF(
         array=array,grid=grid,frequency=2000)
    # performing the beamforming calculation
    beamforming_map = method.solve(sound.CSM)
    
    # DAMAS
    # method = beamlib.beamforming.DAMAS(
    #      array=array,grid=grid,frequency=2000)
    # # performing the beamforming calculation
    # beamforming_map = method.solve(sound.CSM,algo='NNLS')
    
    # plotting the result
    fig, ax, mappable = beamlib.visualize.surface.plot_grid(
        grid.x,grid.y,method.level)
    cbar = beamlib.visualize.surface.add_colorbar(
        fig, mappable, label=r'$\Delta$ source power, dB')
    
    # plotting the array of microphones
    array.plot()
    # defining the colomap/colorbar limits
    mappable.set_clim([-15,0])
    
