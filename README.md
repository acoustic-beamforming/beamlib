# Beamlib

Library for performing acoustic beamforming from signal obtained with an array of microphones.

This is part of the POLA3 project.

## Methods

- classical delay-and-sum algorithm
- DAMAS - https://doi.org/10.1016/j.jsv.2005.12.046
- CLEAN-PSF and CLEAN-SC - https://doi.org/10.2514/6.2007-3436

## Implemented microphone arrays

- Rectangular grid
- Archimedean spiral
- Dougherty log-spiral array
- Arcondoulis array 
- Underbrink

# Usage

First you need to clone this repository in your machine.

You may either load the files directly (package is located in [src/beamlib](/src/beamlib)) or install it using pip (from this folder):

```shell
pip install .
```

Examples on how to use the package are available in the [examples](/examples) folder, with codes with the generation of synthetic signals, calculation of conventional beaforming maps and application of a deconvolution algorithm.
