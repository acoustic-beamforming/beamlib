#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np

from .array import ArrayBase

class Underbrink(ArrayBase):
                 
    def __init__(self,
             Na=8, Nm=8, v=5*np.pi/16, r0=0.1, rmax=0.5,
             array_center=[0,0],z=0.0,center_mic=True):
        """ Planar Underbrink array
        
        As defined in:
            
            Prime, Z., & Doolan, C. (2013). A comparison of popular 
            beamforming arrays. Proceedings of ACOUSTICS 2013
            17–20 Nov, 2013, Victor Harbor, Australia
        
        Default values return an array with 65 microphones centered
        at [x,y,z] = [0.0, 0.0, 0.0].
        
        ARGUMENTS
        ----------
            
            Na: number of spirals
            
            Nm: number of microphones per spiral
            
            v: spiral angle, rad
            
            r0: starting radius in meters, 0.1 m as default
            
            rmax: final radius in meters, 0.5 m as default
            
            array_center: coordinates of the center of the array in meters,
                [0, 0] as default
                
            z: z coordinate of the array (in meters), 0.0 as default
            
            center_mic: boolean to define to add a central microphone,
                True as default
        
        """
        
        v_cot = np.arctan(v)
        r = np.zeros((Na,Nm))
        self.number_microphones = r.size
        self.center = [*array_center,z]
        
        theta = r.copy()
        r[:,0] = r0
        for m in range(1,Na+1):
            for n in range(1,Nm+1):
                if n > 1:
                    r[m-1,n-1] = np.sqrt( (2*n - 3) / (2*Nm - 3) )*rmax
                theta[m-1,n-1] = np.log(r[m-1,n-1]/r0)/v_cot + (m - 1)*2*np.pi/Na
                
        r = r.reshape(self.number_microphones)
        theta = theta.reshape(self.number_microphones)
        
        mic_x = r*np.cos(theta)- array_center[0]
        mic_y = r*np.sin(theta) - array_center[1]
        mic_z = mic_x.copy()*0 + z
        
        microphones = np.vstack((mic_x,mic_y,mic_z)).T
        # adding mic on the center
        if center_mic:
            self.number_microphones += 1
            microphones = np.vstack(
                ([*array_center,z], microphones),
                )
        
        self.name = 'underbrink'
        self.coordinates = microphones
    
