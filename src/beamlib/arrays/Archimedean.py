#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np

from .array import ArrayBase

class Archimedean(ArrayBase):
                 
    def __init__(self,
                 N=64, phi=11*np.pi/2, r0=0.1,rmax=0.5,
                 array_center=[0,0],z=0.0):
        """ Archimedean spiral array
        
        As defined in:
            
            Prime, Z., & Doolan, C. (2013). A comparison of popular 
            beamforming arrays. Proceedings of ACOUSTICS 2013
            17–20 Nov, 2013, Victor Harbor, Australia
        
        Default values return an array with 64 microphones centered
        at [x,y,z] = [0.0, 0.0, 0.0].
        
        ARGUMENTS
        ----------
            
            N: number of microphones
            
            phi: total angle of the spiral sweeps, rad
            
            r0: starting radius in meters, 0.1 m as default
            
            rmax: final radius in meters, 0.5 m as default
            
            array_center: coordinates of the center of the array in meters,
                [0, 0] as default
                
            z: z coordinate of the array (in meters), 0.0 as default
            
            center_mic: boolean to define to add a central microphone,
                True as default
        
        """
        
        self.name = 'archimedean'
        self.number_microphones = N
        self.center = [*array_center,z]
        
        mic_x = np.zeros((N,1))
        mic_y = mic_x.copy()
        
        for n in range(N):
            theta_n = (n -1)*phi/(N - 1)
            r_n = r0 + ((rmax - r0)/phi)*theta_n
            
            mic_x[n] = r_n*np.cos(theta_n)
            mic_y[n] = r_n*np.sin(theta_n)
            
        mic_z = mic_x.copy()*0 + z
        
        self.coordinates = np.hstack(
            (mic_x+array_center[0],mic_y+array_center[1],mic_z))
        
    
