#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Base functions and classes used to build the array

"""

import xml.etree.ElementTree as ET

import numpy as np
import matplotlib.pyplot as plt

from .. import grid as beamlibgrid


class ArrayBase():
        
    def __init__(self,filename=None):
        if filename is not None:
            self.load(filename)
    
    def __call__(self):
        return self.coordinates, self.number_microphones
    
    def __len__(self):
        return self.number_microphones
    
    def plot(self,ax=None):
        """ Function for plotting the array of microphones """
        if ax is None: ax = plt.gca()
        
        microphones_artist = ax.plot(
            self.coordinates[:,0], self.coordinates[:,1],
            marker='.',linestyle='',color='b',
            markerfacecolor='none',
            )
        
        return microphones_artist
        
    def export(self, filename):
        """ Exporting in xml format to be used by acoular library """
        
        header = '<?xml version="1.0" encoding="utf-8"?>\n' + \
                f'<MicArray name="{self.name}">\n'
        with open(filename,'w') as f:
            f.write(header)
            for mic_index, coords in enumerate(self.coordinates):
                f.write(f'\t<pos Name="Point\t{mic_index}" '
                        f'x="{coords[0]:0.6f}" y="{coords[1]:0.6f}" z="{coords[2]:0.6f}"/>\n'
                        )
            f.write('</MicArray>')
    
    def load(self, filename):
        """ Loading array information in xml format """
        
        data = ET.parse(filename)
        root = data.getroot()
        
        self.name = root.attrib['name']
        self.number_microphones = len(root)
        self.coordinates = np.zeros((self.number_microphones,3),)
        
        for m, microphone in enumerate(root):
            self.coordinates[m,:] = \
                [microphone.attrib[dim] for dim in ('x','y','z')]
    
    def qualify(self,frequency,grid=None,source=None,c0=343,
                max_level=0,refinement_factor=1.5):
        """ Estimates beamwidth and Maximun Sidelobe Level (MSL)
        
        Follows the procedure proposed in:
        
            Prime, Zebb, and Con Doolan. "A comparison of popular beamforming
            arrays." Proceedings of the Australian Acoustical Society AAS2013
            Victor Harbor 1 (2013): 5.
        
        First, calculates the array response on the given grid points and 
        frequency. Radial derivative, based on main lobe position, is
        calculated using second order precision.
        
        The main lobe limit is defined by a contour of null radial derivatice,
        calculated with the matplotlib contour function. The beam width
        equals twice the average distance from the main lobe (peak) to the
        vertices of the limiting path.
        
        MSL equals the maximal value outside the delimited region.
        
        This method is only adapted to uniform, square grid
        with dx = dy = constant.
        
        ARGUMENTS
        ----------
        
            frequency: frequency to consider, in Hertz
            
            grid: grid instance. Must contains the scanning grid coordinates
                in meters. Optional, default (None), creates a 101 x 101
                uniform grid, x and y [-0.5,0.5], 1.0 from the array, 
            
            source: tuple with the source coordinates. Optional, default (None)
                selects the grid central point.
            
            c0: speed of sound in m/s. Optional, default is 343 m/s
            
            max_level: due to the nature of the algorithm, for some cases,
                the contour of null derivative does not contours the
                mainlobe, thus, the returned valeu is incorrect. If argument
                `max_level` is provided, calculations will be performed for
                several grid refinements until at least 3 successfull 
                main lobes delimitation. A successful run is one that rerturns
                a MSL lower than `max_level`. Default value (0), uses a
                single refinement.
                
            refinement_factor: factor to be used on automatic grid refinement
                when `min` is provided. Default is 50% (1.5)
        
        RETURNS
        ---------
        
            beamwidth: main lobe beam width in meters. Average of runs with
                different grid refinements if `threshold` is defined
        
            MSL: Maximun Sidelobe Level (MSL), relative to the main lobe.
                Maximun level for the gird points that are located oustide
                the circle that contours the main lobe with radius
                of `beamwidth/2`. Average of runs with different grid
                refinements if `threshold` is defined
        
        """
        
        
        if grid is None:
            grid = beamlibgrid.Uniform(
                n=101,x=[-0.5,0.5],y=[-0.5,0.5],z=1.0)
        if source is None:
            source = [
                (np.min(coords) + np.max(coords))/2
                    for coords in grid.coordinates.T
            ]
        
        beamwidths, MSLs = [], []
        
        # smaller than grid actual size so the first iteration is with non refined
        grid_size = float(grid.x.size)/refinement_factor
        
        while len(MSLs) < 3:
            
            # adding twice as many points
            grid_size *= refinement_factor
            grid_ref = beamlibgrid.Uniform(
                n=int(grid_size),
                x=[grid.x[0],grid.x[-1]],
                y=[grid.y[0],grid.y[-1]],
                z=grid.coordinates[0,-1])
            grid = grid_ref
            map_shape = [grid.x.size,grid.y.size]
            
            W = calc_array_response(
                array=self,grid=grid,frequency=frequency,source=source,c0=c0)
            
            W_power = 10*np.log10(W*W.conj()/np.max(W*W.conj()))
            
            W_map = np.real(W).reshape(map_shape)
            
            # selecting the main lobe
            index_main_lobe = np.argmax(W)
            coords_main_lobe = grid.coordinates[index_main_lobe,:-1]
            x_main_lobe, y_main_lobe = coords_main_lobe
            
            delta_X = grid.coordinates[:,0] - x_main_lobe
            delta_Y = grid.coordinates[:,1] - y_main_lobe
            R = np.sqrt(delta_X**2 + delta_Y**2)
            Theta = np.arctan2(delta_Y,delta_X)
            theta = Theta.reshape(map_shape)
            
            dx = grid.x[1] - grid.x[0]
            dy = grid.y[1] - grid.y[0]
            
            # second order accurate central differences in the interior points
            # and forward or backwards differences at the boundaries
            dW_dx = np.gradient(W_map, dx, axis=1, edge_order=2)
            dW_dy = np.gradient(W_map, dy, axis=0, edge_order=2)
            # convert to radial derivative
            dW_dr = dW_dx*np.cos(theta) + dW_dy*np.sin(theta)
            
            # creating a contour to get the region of the main lobe
            fig, ax = plt.subplots()
            dWdr_contour = ax.contour(
                grid.x, grid.y,dW_dr,
                levels=[0],linewidths=[1.0],colors=['k'],
                )
            # define the main lobe regions and calculate beamwidth and MSL
            # getting the contour - the one that contains the main lob
            paths = dWdr_contour.collections[0].get_paths()
            del dWdr_contour
            plt.close(fig)
            
            contains_lobe = np.zeros(len(paths),dtype=bool)
            areas = contains_lobe.copy() + float('Inf')
            for index_path, path in enumerate(paths):
                contains_lobe[index_path] = path.contains_point(coords_main_lobe)
                if contains_lobe[index_path]:
                    areas[index_path] = calc_path_area(path)
            # selecting the path as the smallest polygon that contains the source
            try:
                index_selected_path = np.arange(len(paths))[contains_lobe][
                    np.argmin(areas[contains_lobe])
                    ]
                path_main_lobe = paths[index_selected_path]
                
                vertices = path_main_lobe.vertices
                radius = np.sqrt(
                    np.sum(
                        (vertices - coords_main_lobe*np.ones((len(vertices),1)))**2,
                        axis=1
                        )
                    )
            except(ValueError):
                # in the case where no closed contour could be defined,
                # defining the radius of the inscribed circle and selecting MSL 
                # based on that
                radius = (1/2)*np.min(
                    np.sqrt(
                        [(np.max(m) - np.min(m))**2.0 
                            for m in (grid.x, grid.y)
                            ]
                        )
                    )

            beamwidth = 2*np.mean(radius)
            mask_outside_main_lobe = R >= beamwidth/2
            MSL = np.max(np.real(W_power[mask_outside_main_lobe]))
        
            # copy values to force stop after 1st iteration
            if max_level == 0: MSLs = [MSL,MSL]
            
            if MSL <= max_level:
                MSLs.append(MSL)
                beamwidths.append(beamwidth)
        
        beamwidth = np.mean(beamwidths)
        MSL = np.mean(MSLs)
        
        return beamwidth, MSL


def calc_array_response(array,grid,frequency,source,c0=343):
    """ Calculate the array response for a given grid
    
    Formulation from:
    
        Underbrink, James R. (2002). “Aeroacoustic Phased Array Testing in
        Low Speed Wind Tunnels”. Aeroacoustic Measurements. Ed. by
        Thomas J. Mueller. Berlin: Springer. Chap. 3, pp. 98–217.
        ISBN: 3-540-41757-5.
    
    Used to estimate the beamwidth and the Maximun Sidelobe Level (MSL).
    
    ARGUMENTS
    ----------
    
        array: array instance
        
        grid: grid instance
        
        frequency: frequency position
        
        source: tuple with the source coordinates
        
        c0: speed of sound in m/s. Optional, default is 343 m/s
    
    RETURNS
    ----------
    
        W: 1D array with the effect of the beamformer on the signal spectrum.
            Format (grid.N,1), complex
    
    """
    
    n_mics = array.number_microphones
    n_grid = grid.N
    
    omega = 2*np.pi*frequency # angular frequency
    
    distance = lambda v1, v2: np.sqrt(np.sum((v1 - v2)**2,axis=1))
    
    # forcing convert to numpy array and copying it to
    # vectorize operations
    source = np.array(source)
    origin = np.sum(array.coordinates,axis=0)
    
    # distance from source to origin (n_grid,3)
    rs = distance(source*np.ones((n_grid,1)),origin*np.ones((n_grid,1)))
    
    # distance from origin to focus points (n_grid,3)
    rp = distance(origin*np.ones((n_grid,1)),grid.coordinates)
    
    # distance from microphones to source (n_mics,3)
    rn = distance(source*np.ones((n_mics,1)),array.coordinates)
    
    # complex array response (n_grid,n_mics), contributions of all mics
    # (on axis 1) are summed later
    W = np.zeros((n_grid,n_mics),dtype=np.complex128)
    
    # loop of focus points
    for index_grid, focus_coords in enumerate(grid.coordinates):
        # distance from microphones to focus point
        rn_ = distance(array.coordinates,focus_coords*np.ones((n_mics,1)))
        W[index_grid,:] = (rs[index_grid]/rn)*np.exp(
            1j*(omega/c0)*(
                (rs[index_grid] - rp[index_grid]) - (rn - rn_)
                )
            )

    W = np.sum(W,axis=1)

    return W


def calc_path_area(path):
    """ Calculate the area of a 2D matplotlib path
    
    Uses the Shoelace formula. Not adapted to complex polygons
    (self-intersecting). From:
    
        https://stackoverflow.com/questions/24467972/calculate-area-of-polygon-given-x-y-coordinates
    
    """
    x, y = path.vertices.T
    return 0.5*np.abs(np.dot(x,np.roll(y,1))-np.dot(y,np.roll(x,1)))
