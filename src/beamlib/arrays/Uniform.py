#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np

from .array import ArrayBase

class Uniform(ArrayBase):
                 
    def __init__(self,
                 array_side=1.0,n_grid=10,
                 array_center=[0,0],z=0.0,):
        """ Planar uniform rectangular array
        
        Default values return an array with 100 microphones centered
        at [x,y,z] = [0.0, 0.0, 0.0].
        
        ARGUMENTS
        ----------
            
            array_side: size of the array in meter, 1.0 m as default
                If a single value, a square array will be generated,
                Use a list to define non-square array [Lx,Ly]
                
            n_grid: grid discretization in each direction, 10 as default.
                If a single value, uniform discretization will be used,
                use a list to define non-uniform discretization [nx,ny]
                
            array_center: coordinates of the center of the array in meters,
                [0, 0] as default
                
            z: z coordinate of the array (in meters), 0.0 as default
        
        """
        
        self.name = 'uniform'
        self.center = [*array_center,z]
        
        if not isinstance(n_grid,list):
            n_grid = [n_grid,n_grid]
            
        if not isinstance(array_side,list):
            array_side = [array_side,array_side]
        
        
        self.number_microphones = n_grid[0]*n_grid[1]
        
        half_side = [L/2 for L in array_side]
        
        mic_x = np.linspace(
            array_center[0] - half_side[0],
            array_center[0] + half_side[0],
            num=n_grid[0]
            )
        mic_y = np.linspace(
            array_center[1] - half_side[1],
            array_center[1] + half_side[1],
            num=n_grid[1]
            )
        
        mic_X, mic_Y = np.meshgrid(mic_x,mic_y)
        mic_Z = mic_X.copy()*0 + z
        
        # flatten coordinates
        self.coordinates = np.vstack(
            [arr.reshape(self.number_microphones) for 
                 arr in (mic_X,mic_Y,mic_Z)]
            ).T
        
    
