#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np

from .array import ArrayBase

class Arcondoulis(ArrayBase):
                 
    def __init__(self,
                 N=64, phi=11*np.pi/2, ex=0.9, ey=0.9,
                 r0=0.1,rmax=0.5,
                 array_center=[0,0],z=0.0):
        """ Arcondoulis array
        
        As defined in:
            
            Prime, Z., & Doolan, C. (2013). A comparison of popular 
            beamforming arrays. Proceedings of ACOUSTICS 2013
            17–20 Nov, 2013, Victor Harbor, Australia
        
        Default values return an array with 64 microphones centered
        at [x,y,z] = [0.0, 0.0, 0.0].
        
        ARGUMENTS
        ----------
            
            N: number of microphones
            
            phi: total angle of the spiral sweeps, rad
            
            ex, ey: antenna squashing in x and y
            
            r0: starting radius in meters, 0.1 m as default
            
            rmax: final radius in meters, 0.5 m as default
            
            array_center: coordinates of the center of the array in meters,
                [0, 0] as default
                
            z: z coordinate of the array (in meters), 0.0 as default
        
        """
        
        self.name = 'arcondoulis'
        self.number_microphones = N
        self.center = [*array_center,z]
        
        a = r0*(N/(ex*N + 1))
        b = (1/phi)*np.log(rmax/(
            a*np.sqrt(
                ((1 + ex)*np.cos(phi))**2 + ((1 + ey)*np.sin(phi))**2
                )
            )
        )
        
        mic_x = np.zeros((N,1))
        mic_y = mic_x.copy()
        mic_z = mic_x.copy()*0 + z
        
        for n in range(N):
            theta_n = (n -1)*phi/(N - 1)
            mic_x[n] = ((n + ex*N)/N)*a*np.cos(theta_n)*np.exp(b*theta_n)
            mic_y[n] = ((n + ey*N)/N)*a*np.sin(theta_n)*np.exp(b*theta_n)
        
        self.coordinates = np.hstack(
            (mic_x+array_center[0],mic_y+array_center[1],mic_z))
        
    
