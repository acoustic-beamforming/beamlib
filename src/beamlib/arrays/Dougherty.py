#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np

from .array import ArrayBase

class Dougherty(ArrayBase):
                 
    def __init__(self,
                 N=64, v=16.5*np.pi/32,
                 r0=0.1,rmax=0.5,
                 array_center=[0,0],z=0.0):
        """ Dougherty log-spiral array
        
        As defined in:
            
            Prime, Z., & Doolan, C. (2013). A comparison of popular 
            beamforming arrays. Proceedings of ACOUSTICS 2013
            17–20 Nov, 2013, Victor Harbor, Australia
        
        Default values return an array with 64 microphones centered
        at [x,y,z] = [0.0, 0.0, 0.0].
        
        ARGUMENTS
        ----------
            
            N: number of microphones
            
            v: spiral angle, rad. It represents the constant angle
                at which radii from the origin of the spiral are cut by
                the spiral curve
            
            r0: starting radius in meters, 0.1 m as default
            
            rmax: final radius in meters, 0.5 m as default
            
            array_center: coordinates of the center of the array in meters,
                [0, 0] as default
            
            z: z coordinate of the array (in meters), 0.0 as default
            
            center_mic: boolean to define to add a central microphone,
                True as default
        
        """
        
        self.name = 'dougherty'
        self.number_microphones = N
        self.center = [*array_center,z]
        
        mic_x = np.zeros((N,1))
        mic_y = mic_x.copy()
        
        cot_v = 1/np.tan(v)
        cot_v_square = cot_v**2
        
        l_max = (rmax/r0 - 1)*r0*np.sqrt(1 + cot_v_square)/cot_v
        
        par_term = cot_v/(r0*np.sqrt(1 + cot_v_square))
        
        for n in range(N):
            l_n = (n -1)*l_max/(N - 1)
            
            theta_n = (1/cot_v)*np.log(1 + par_term*l_n)
            r_n = r0*np.exp( cot_v*theta_n )
            
            mic_x[n] = r_n*np.cos(theta_n)
            mic_y[n] = r_n*np.sin(theta_n)
            
        mic_z = mic_x.copy()*0 + z
        
        self.coordinates = np.hstack(
            (mic_x+array_center[0],mic_y+array_center[1],mic_z))
        
    

    
