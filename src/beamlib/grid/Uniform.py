#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Uniform rectangular grid

    n: number of discretization points
    x: tuple with the x-limits of the scanning grid
    y: tuple with the y-limits of the scanning grid
    z: z coordinate of the focal point
    
    
"""

import numpy as np

from .grid import GridBase

class Uniform(GridBase):
                 
    def __init__(self,n=15,x=[-0.5,0.5],y=[-0.5,0.5],z=0.0):
        
        self.name = 'Uniform'
        self.N = n*n
        
        # grid coordinates in x and y
        self.x = np.linspace(x[0],x[1],num=n)
        self.y = np.linspace(y[0],y[1],num=n)
        
        # defining the 2D mesh
        scan_X, scan_Y = np.meshgrid(self.x,self.y)
        scan_Z = scan_X.copy()*0 + z
        
        # flatten coordinates
        self.coordinates = np.vstack(
            [arr.reshape(self.N) for arr in (scan_X,scan_Y,scan_Z)]
            ).T
    

    def refine(self,n=15):
        """ Returned a refined instance of the grid """
        
        x = [self.x[0],self.x[-1]]
        y = [self.y[0],self.y[-1]]
        z = self.coordinates[0,-1]
        
        return Uniform(n=n,x=x,y=y,z=z)
    