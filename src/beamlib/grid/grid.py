#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Base functions and classes used to build the scan grids

"""

import xml.etree.ElementTree as ET

import numpy as np

class GridBase():
    
    def __init__(self,filename=None):
        if filename is not None:
            self.load(filename)

    def __call__(self,):
        return self.coordinates
    
    def __len__(self,):
        return self.N
    
    def export(self, filename):
        """ Exporting in xml format"""
        
        header = '<?xml version="1.0" encoding="utf-8"?>\n' + \
                f'<Grid name="{self.name}">\n'
        with open(filename,'w') as f:
            f.write(header)
            for mic_index, coords in enumerate(self.coordinates):
                f.write(f'\t<pos '
                        f'x="{coords[0]:0.6f}" y="{coords[1]:0.6f}" z="{coords[2]:0.6f}"/>\n'
                        )
            f.write('</Grid>')
    
    def load(self, filename):
        """ Loading grid information in xml format """
        
        data = ET.parse(filename)
        root = data.getroot()
        
        self.name = root.attrib['name']
        self.N = len(root)
        self.coordinates = np.zeros((self.N,3),)
        
        for m, grid_point in enumerate(root):
            self.coordinates[m,:] = \
                [grid_point.attrib[dim] for dim in ('x','y','z')]
        
        if self.name == 'Uniform':
            # returning an Uniform class instance
            return _instantiate(self)
        

def _instantiate(base):
    """ Instancing GridBase child classes """
    from .Uniform import Uniform

    if base.name == 'Uniform':
        x = [fun(base.coordinates[:,0]) for fun in (np.min,np.max)]
        y = [fun(base.coordinates[:,1]) for fun in (np.min,np.max)]
        z = base.coordinates[0,-1]
        n = int(np.sqrt(base.N))
        
        return Uniform(n=n,x=x,y=y,z=z)