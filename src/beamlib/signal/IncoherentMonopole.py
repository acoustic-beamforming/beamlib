#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Generates microphone signal in the time domain for N monopole sources
with random shapes with no noise.

Considers a homogeneous medium with no obstacles and isotropic propagation.

"""

import numpy as np

from .signal import SignalBase

class IncoherentMonopole(SignalBase):
    
    def _generate(self,sampling_frequency=51200,sampling_time=1.0,):
        """ Signal generation
        
        ARGUMENTS
        ----------
        
            array: Array class containing the position of the microphones
            sampling_frequency: sampling_frequency in Hertz. Optional,
                                default is 51200 Hz
            sampling_time: sampling time in seconds. Optional,
                           default is 1.0 second.
        
        """
        
        self.domain = 'time'
        self.sampling_frequency = sampling_frequency
        
        # generating signals in time and calculating the fourier
        # transform after
        sampling_period = 1/sampling_frequency
        number_samples = int(sampling_frequency*sampling_time)
        
        self.signal = np.zeros(
            (self.number_microphones,number_samples),dtype=np.float64)
        
        for source_index, (source, amplitude) in enumerate(
                zip(self.coordinates,self.amplitudes)):
            
            # generating source sound signal (extra points to account for
            # the delay to arrive in microphones)
            source_pressure = np.random.normal(
                loc=0, scale=amplitude, size=number_samples + 1000)
            
            for mic_index, microphone in enumerate(self.array.coordinates):
                r = np.sqrt(np.sum( (source - microphone)**2) )
                # time delay
                delta_t = r/self.c0
                # number of samples that represent such time delay
                delay_samples = int(delta_t/sampling_period)
                # adding sources (with attenuarion + time delay)
                attenuation = 1/(4*np.pi*r)
                
                self.signal[mic_index,:] += source_pressure[
                    delay_samples:(number_samples+delay_samples)]*attenuation
        
        
    def __call__(self,):
        return self.signal
