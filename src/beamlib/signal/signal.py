#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Base functions and classes used by the signal generation scripts

"""

import numpy as np
import scipy.signal

import matplotlib.pyplot as plt

class SignalBase():
    
    def __init__(self,array,sources,amplitudes=1.0,c0=343,rho0=1.225,
                 data=None,**kwargs):
        """ Initiaing and calling the signal generation methods
        
        ARGUMENTS
        ----------
        
            array: Array class containing the position of the microphones
            sources: list of tuples with the position of the sources in
                     meter in form [x,y,z]
            amplitudes: list with the amplitudes of each source. If 
                        single value, all sources will be given the same
                        amplitude. Default is 1.0.
            c0: sound speed in m/s. Optional, default is 343 m/s.
            rho0: fluid density in kg/m3. Optional, default is 1.225 kg/m3.
            data: array with the sound generated. If defined, data is not 
                  recalculated. This allows to create a dummy Signal class
                  from recorded or previously generated signals.
                  Optional is None (data is generated)
            **kwargs: arguments passed to each specific class
        
        RETURNS
        ----------
        
            ##none##
        
        """
        
        self.coordinates = np.array(sources)
        self.number_sources = self.coordinates.shape[0]
        
        self.number_microphones = len(array)
        self.array = array
        
        if type(amplitudes) is not list or len(amplitudes) == 1:
            amplitudes = [amplitudes]*self.number_sources
        self.amplitudes = amplitudes
        
        self.c0 = c0
        self.rho0 = rho0
        
        # call the method for calculating the signals
        if data is None:
            self._generate(**kwargs)
        else:
            self.signal = data
            # use the kwargs variables to set the remaining attributes
            # defined in the case of a default signal generation
            for key, value in kwargs.items():
                setattr(self,key,value)
    
    
    def calc_CSM(self,frequency=None,nfft=1024):
        """ Calculate the cross-spectral matrix (CSM)
        
        It considers the attribute `domain` to performe the calculation
        considering a signal that is already in the spectral domain
        (freq) or in the time domain (time).
        
        
        ARGUMENTS
        ----------
        
            frequency: selected frequency for the calculation of the
                       CSM. If not directly present in the list of
                       calculated frequencies, closest value is selected
            nfft: number of points used in the  cross power spectra.
                  Only used for time signals.
        
        RETURNS
        ----------
        
            the CSM
        
        """
        
        if self.domain == 'freq':
            # calculate the cross-spectral matrix
            self.CSM = np.matmul(self.signal,self.signal.conj().T)
        else:
            
            if type(frequency) is not list or len(frequency) == 1:
                self.CSM = _single_frequency_CSM(
                    self.signal,
                    frequency=frequency,
                    sampling_frequency=self.sampling_frequency,
                    nfft=nfft,
                    overlap=0.5,
                    window='hann')
            else:
                # selecting the frequency of interest
                freq_bins = np.fft.fftfreq(nfft)*self.sampling_frequency
                index_freq = np.argmin(np.abs(frequency - freq_bins))
                
                # calculating the CSM
                # TODO: this calculation takes a lot of time! one must
                #       optimize and remove the loops
                print('\nCalculating the CSM matrix from a time signal...')
                self.CSM = np.zeros(
                    (self.number_microphones,self.number_microphones),
                    dtype=np.complex128)
                
                for mic_1 in range(self.number_microphones):
                    
                    if mic_1 % 10 == 0:
                        print(f'\t microphone {mic_1}/{self.number_microphones-1}')
                    
                    for mic_2 in range(self.number_microphones):
                        _, CSD = scipy.signal.csd(
                            self.signal[mic_1,:],
                            self.signal[mic_2,:],
                            fs=self.sampling_frequency,
                            window='hanning', 
                            noverlap=int(0.9*nfft),
                            nperseg=nfft,
                            return_onesided=True,
                            scaling='spectrum',
                            average='mean')
                        # extracting only one frequency
                        self.CSM[mic_1,mic_2] = CSD[index_freq]
                print('Done.')
        
        return self.CSM
    
    
    def update(self,new_sources,amplitudes=None,**kwargs):
        """ Recalculate signals for different sources. """
        
        self.coordinates = np.array(new_sources)
        self.number_sources = self.coordinates.shape[0]

        if amplitudes is None:
            # if not defined, using same amplitude
            amplitudes = [1.0]*self.number_sources
        self.amplitudes = amplitudes

        self._generate(**kwargs)
    
    
    def cartography(self,grid):
        """ Building a 2D sources map based on sources coordinates """
        
        source_map = np.zeros(grid.N)
        
        for i, source in enumerate(np.array(self.coordinates)):
            index_source = np.argwhere((grid.coordinates == source).all(-1) )
            source_map[index_source] = self.amplitudes[i]
            
        return source_map.reshape((grid.x.size, grid.y.size))
    
    
    def plot(self,ax=None):
        """ Function for plotting the sources positions. """
        if ax is None: ax = plt.gca()
        
        microphones_artist = ax.plot(
            self.coordinates[:,0], self.coordinates[:,1],
            marker='x',linestyle='',color='g',
            linewidth=0.5,
            markersize=3.0,
            markerfacecolor='none',
            )
        
        return microphones_artist


def _single_frequency_CSM(signals,**kwargs):
    """ Average (Welch) cross-spectral matrix a single frequency
    
    ARGUMENTS
    ----------
    
        signals: array in format [n_probes,n_samples] with the data to be 
                 treated. All signals must be of same size and generated
                 using the same sampling frequency
        **kwarg: calculation parameters as defined in `_single_frequency_fft`
    
    RETURNS
    ----------
    
        CSM: cross-spectral matrix in format [n_probes,n_probes]
    
    """
    
    number_probes = signals.shape[0]
    
    # calculating the individual fourier transform of the signals
    signals_spectra = []
    for x in signals:
       signals_spectra.append(
           _single_frequency_fft(x,**kwargs)
           )
    
    # considering all combinations of signals
    CSM = np.zeros((number_probes,number_probes),dtype=np.complex128)
    for i in range(number_probes):
        for j in range(number_probes):
            XY_k = np.conjugate(signals_spectra[i])*signals_spectra[j]
            CSM[i,j] = np.mean(XY_k,axis=0)
    
    return CSM


def _single_frequency_fft(
        x,frequency,sampling_frequency,nfft,overlap=0.5,window='hann'):
    """ Windowed Fourier transform of a signal at a single frequency
    
    This code is inspired in scipy's implementation of the Welch algoritm
    available at:
        
        https://github.com/scipy/scipy/blob/v1.6.1/scipy/signal/spectral.py#L291-L454
    
    ARGUMENTS
    ----------
    
        x: data
        frequency: frequency of interest in Hertz. Calculation will consider
                   the closest frequency based on the number of points of
                   the fft and the sampling frequency
        sampling_frequency: sampling frequency in Hertz
        nfft: number of points for the FFT (used segment size)
        overlap: ratio of overlap (from 0 to 1). Optinal, default is 0.5
                 (50% of overlap)
        window: name of the window used for the segments sampling. Optional,
                `hann` as default. To see all available windows, check:
                    
            scipy.signal.windows.windows.py
            
                or
            
            https://docs.scipy.org/doc/scipy/reference/signal.windows.html
    
    RETURNS
    ----------
    
        X_k_avg: Fourier transform of the input signal at the selected
                 frequency, vector array considering all segments.
                 For obtaining the average autopower (as in Welch method),
                 do the following:
                     
                     >>> XX = np.conjugate(X_k)*X_k
                     >>> X_k_avg = np.mean(XX,axis=0)
                     
                These steps are available in `_single_frequency_welch`
    
    """
    
    n_overlap = int(nfft*overlap)
    n_step = nfft - n_overlap
    
    win = scipy.signal.spectral.get_window(window, nfft)
    scale = 1.0 / win.sum()**1.0 # **2
    
    # TODO: this is calcualted everytime the function is called, maybe 
    #       reorganized the code so it is only done once
    freq_vec = np.fft.fftfreq(nfft)[:nfft//2]*sampling_frequency
    k = np.argmin(np.abs(frequency - freq_vec))
    
    shape = x.shape[:-1] + ((x.shape[-1] - n_overlap)//n_step, nfft)
    strides = x.strides[:-1] + (n_step*x.strides[-1], x.strides[-1])
    x = np.lib.stride_tricks.as_strided(x, shape=shape, strides=strides)
    # removing average for all segments
    x = scipy.signal.signaltools.detrend(x, type='constant', axis=-1)
    # apply window
    x = win * x
    
    # calculate single frequency FFT and normalize due to the windowing
    X_k = np.sum(
        x*np.exp(-2j*np.pi*k*np.arange(nfft)/nfft), axis=1
        )
    X_k *= scale*np.sqrt(2.0)
    
    return X_k


def _single_frequency_welch(x,**kwargs):
    """ Averaged (Welch) autopower of a signal at a single frequency
    
    ARGUMENTS
    ----------
    
        x: signals to be considered
        **kwarg: calculation parameters as defined in `_single_frequency_fft`
    
    RETURNS
    ----------
    
        XX_k_avg: average auto-power at the selected frequency
    
    """
    
    X_k = _single_frequency_fft(x,**kwargs)
    XX_k = np.conjugate(X_k)*X_k
    XX_k_avg = np.mean(XX_k,axis=0)
    
    return XX_k_avg


def _single_frequency_csd(x,y,**kwargs):
    """ Averaged (Welch) cross-spectrum of two signals at single frequency
    
    ARGUMENTS
    ----------
    
        x, y: signals to be considered. Must be of same size and generated
              using the same sampling frequency
        **kwarg: calculation parameters as defined in `_single_frequency_fft`
    
    RETURNS
    ----------
    
        XY_k_avg: average cross-spectral power at the selected frequency
    
    """
    
    X_k = _single_frequency_fft(x=x, **kwargs)
    Y_k = _single_frequency_fft(x=y, **kwargs)
    
    XY_k = np.conjugate(X_k)*Y_k
    XY_k_avg = np.mean(XY_k,axis=0)
    
    return XY_k_avg
