#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Generates microphone signal in the spectral domain for N monopole sources
with sinusoidal shapes with no noise.

"""

import numpy as np

from .signal import SignalBase

class TonalMonopole(SignalBase):
    
    def _generate(self,frequencies,amplitudes=1.0,):
        """ Signal generation
        
        ARGUMENTS
        ----------
        
            array: Array class containing the position of the microphones
            frequencies: single value or list indicating the frequency
                        of each source
        
        """
        
        self.domain = 'freq'
        
        if type(frequencies) is not list or len(frequencies) == 1:
            frequencies = [frequencies]*self.number_sources
        
        # microphones spectral pressure
        self.signal = np.zeros(
            (self.number_microphones,1), dtype=np.complex128)
    
        # generating the sources signal (spectral)
        for source_index, (source, frequency, amplitude) in enumerate(
                zip(self.coordinates,frequencies,self.amplitudes)):
            for mic_index, microphone in enumerate(self.array.coordinates):
                # distance from source to microphone
                r = np.sqrt( np.sum( (source - microphone)**2 ) )
                # angular frequency
                omega = 2*np.pi*frequency
                # wave number
                k = omega/self.c0
                # accounting for sound atenuation
                q = amplitude*self.rho0*omega/(4*np.pi*r)
                # monopole source spectra
                source_pressure = q*np.exp(-1j*k*r)
                
                self.signal[mic_index,] += source_pressure
    
    
    def __call__(self,):
        return self.signal