#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Functions used to visualize the result

"""

import matplotlib.pyplot as plt
from matplotlib.ticker import FixedLocator

FIGURE_SIZE=(8/2.54,6/2.4)

import numpy as np


def plot_grid(x, y, Z, ax=None, cmap='hot_r', rasterized=False):
    """ Plot array Z as a rectangular grid with no interpolation
    
    ARGUMENTS
    ----------
    
        x: grid discretization in x, `nx` values
        
        y: grid discretization in y, `ny` values
        
        Z: numpy array with the values to be ploted. Array format is either
           [nx, ny] or [nx*ny, 1]
           
        ax: axis where to plot the grid. Optional, if None a new figure and
            axis will be created
            
        cmap: colormap. Default is reverse hot colormap (`hot_r`), with white
            at the beginning and black at the end
            
        rasterized: make imshow rasterized, that is, map will be in bitmap
            when saving in vectorial format. Optional, default is false
    
    RETURNS
    ----------
    
        fig: matplotlib Figure object that contains the plot
        
        ax: matplotlib Axis object that contains the plot
        
        mappable: matplotlib AxesImage with the image plot
    
    """
    nx, ny = len(x), len(y)
    if Z.shape[0] == nx*ny:
        Z = Z.reshape(nx,ny)
    
    # complex cannot be plotted, so converting it to real
    # TODO: add the possibility to make two plots (real and
    #       complex parts as two distinct plots)
    if np.iscomplexobj(Z):
        Z = np.real(Z)
    
    dx, dy = x[1]-x[0], y[1]-y[0]
    Lx, Ly = x[-1]-x[0], y[-1]-y[0]
    
    x_grid_limits = np.arange(x[0]-dx/2,x[-1]+dx/2,step=dx)
    y_grid_limits = np.arange(x[0]-dy/2,y[-1]+dy/2,step=dy)
    
    if ax is None:
        # creating a new axis
        fig, ax = plt.subplots(
            figsize=FIGURE_SIZE,constrained_layout=True)
    else:
        fig = plt.gcf()
        
    ax.set_aspect(Lx/Ly)
    
    mappable = ax.imshow(
        Z,
        interpolation='antialiased',
        cmap=cmap,
        extent=[x[0]-dx/2,x[-1]+dx/2,
                y[0]-dy/2,y[-1]+dy/2],
        origin='lower',
        rasterized=rasterized,
        )
    
    ax.xaxis.set_minor_locator(FixedLocator(x_grid_limits))
    ax.yaxis.set_minor_locator(FixedLocator(y_grid_limits))
    ax.grid(which='minor',color='gray',linestyle='-',linewidth=0.5)
    
    ax.tick_params(axis='both', which='major', labelsize=6)
    ax.tick_params(axis='both', which='minor', size=0)
    
    return fig, ax, mappable


def add_colorbar(fig,mappable,ax=None,label=None,**cbarargs):
    """ Add colorbar to a figure for a given mappable """
    
    # if axes is not given, selecting the latest one
    if ax is None: ax = plt.gca()

    default_props = dict(shrink=0.8, fraction=0.05)
    for key, value in default_props.items():
        if key not in cbarargs:
            cbarargs[key] = value
    
    cbar = fig.colorbar(mappable, ax=ax, **cbarargs)
    cbar.ax.tick_params(labelsize=6)
    if label is not None:
        cbar.set_label(label, size=7)
    return cbar