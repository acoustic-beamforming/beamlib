#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
"""

from .arrays import __init__

from .grid import __init__

from .beamforming import __init__

from .signal import __init__

from .visualize import __init__



import os
from multiprocessing import current_process

def check_version():
    """ Displays Beamlib's version on the terminal """
    is_main = current_process().name == 'MainProcess' 
    if is_main:
        VERSION = os.popen(
            'git --git-dir={:}/../.git describe --tags'.format(
                os.path.dirname(__file__))
                ).read()
        print(f'Beamlib version: {VERSION}')
