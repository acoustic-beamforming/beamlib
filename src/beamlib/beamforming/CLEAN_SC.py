#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

CLEAN-SC deconvolution method

As defined in:
            
    Pieter Sijtsma. "CLEAN Based on Spatial Source Coherence,"
    AIAA 2007-3436. 13th AIAA/CEAS Aeroacoustics Conference
    (28th AIAA Aeroacoustics Conference). May 2007. 
    https://doi.org/10.2514/6.2007-3436
        
        
"""


import numpy as np

from .beamformer import BeamformerBase
from .beamformer import vector_prod, calculate_level


class CLEAN_SC(BeamformerBase):
    
    def set_parameters(self):
        self.name = 'CLEAN-SC'
    
    def solve(self,CSM,it_max=2000,relax=0.9,convergence=1e-10,
              remove_auto=True,verbose=False):
        """ CLEAN-SC deconvolution method
        
        First calculates the convetional beamforming map and later replace
        the peaks by artificial beams iteratively.
        
        The frequency of the CSM must correspond the one used for calculating
        the steering vectors.
        
        ARGUMENTS
        ----------
        
            CSM: cross-spectral matrix, shape [M,M]
            it_max: maximal number of iterations, only considered in the
                    Gauss-Seidel algorithm. Optional, default is 100
            relax: relaxation factor, only considered in the
                   Gauss-Seidel algorithm. Optional, default is 1.0
            convergence: convergence criteria for stopping the loop
            remove_auto: boolean indicating if diagonal removal is performed.
                         Optional, default is True
            verbose: print on terminal the convergence of the iterative
                     solution, only considered in the Gauss-Seidel algorithm.
                     Optional, default is False
        
        RETURNS
        ----------
        
            A: source autopower matrix, shape [N,N]
        
        where M is the total number of microphones in the array and N is the
        total number of scan points in the focal plan.
        
        """
        
        # degraded CSM
        D = CSM.copy()
        clean = np.zeros((self.number_scan_points,1),dtype=np.complex128)
        
        # Initialise break criterion
        sum_D0 = np.sum( np.abs(CSM) )
        sum_D = sum_D0
        
        for it in range(it_max):
            
            if verbose and it % 10 == 0:
                # displaying the header
                print('{:>5} {:>8} {:>12}'.format('i','eta_max','delta |D|'))
            
            # calculate "dirty" map from degraded CSM
            dirty = clean.copy()*0.0
            # loop of sources
            for j in range(self.number_scan_points):
                dirty[j] = vector_prod(self.weighted[:,j],D)
            
            # peak in dirty map
            j_max = np.argmax(dirty)
            P_max = dirty[j_max]
            
            # steering and weighted vector for the peak location
            e_max = np.expand_dims(self.steering[:,j_max].copy(), axis=1)
            w_max = np.expand_dims(self.weighted[:,j_max], axis=1)
            
            # iterative solution of the source component h
            h = e_max.copy()
            for i_h in range(50):
                hOldValue = h.copy()
                
                hh = h.dot(h.conj().T)
                # hh diagonal
                H = np.identity(self.number_microphones)*np.diag(hh)
                
                wHw = vector_prod(w_max,H)
                Dw = D.dot(w_max)
                Hw = H.dot(w_max)
                
                h = (Dw/P_max + Hw)/np.sqrt(1 + wHw)
                
                if np.sqrt( np.sum( (h - hOldValue)**2 ) ) < 1e-6:
                    break
            
            hh = h.dot(h.conj().T)
            # degraded cross spectral matrix
            D -= relax*P_max*hh
            if remove_auto: np.fill_diagonal(D,0.0)
            
            # update clean map with clean beam at peak source location
            clean[j_max] += relax*P_max
            
            # stop if the degraded CSM contains more information
            # than in the previous iteration
            sum_D0 = np.sum( np.abs(D) )
            delta_sum_D = sum_D - sum_D0
            if verbose:
                print(f'{it:5d} {j_max:8d} {delta_sum_D:12.4e}')
            if delta_sum_D < convergence:
                break
            
            sum_D = sum_D0
        
        self.A = clean + dirty
        
        if verbose: print('\nDone\n')
        
        if remove_auto:
            self.A /= self.number_microphones*(self.number_microphones - 1)
        
        A_real = np.real(self.A)
        self.level = calculate_level(A_real)
        
                
        return self.A