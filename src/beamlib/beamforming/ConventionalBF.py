#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Conventional beamforming implementation

"""


import numpy as np

from .beamformer import BeamformerBase
from .beamformer import vector_prod, calculate_level


class ConventionalBF(BeamformerBase):
    
    def set_parameters(self):
        self.name = 'CBF'
    
    def solve(self,CSM):
        """ Calculating the least-squares solution for the delay-and-sum
        conventional beamforming formulation
        
        The frequency of the CSM must correspond the one used for calculating
        the steering vectors.
        
        ARGUMENTS
        ----------
        
            CSM: cross-spectral matrix, shape [M,M]
        
        RETURNS
        ----------
        
            A: autopower matrix, shape [N,N]
        
        where M is the total number of microphones in the array and N
        is the total number of scan points in the focal plan.
        
        """
        
        # source autopower
        self.A = np.zeros((self.number_scan_points,1), dtype=np.complex128)
        
        for n in range(self.number_scan_points):
            self.A[n,] = vector_prod(self.weighted[:,n],CSM)
        
        A_real = np.real(self.A)
        self.level = calculate_level(A_real)
        
        return self.A