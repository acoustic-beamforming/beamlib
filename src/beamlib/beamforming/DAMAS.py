#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

DAMAS deconvolution method

As defined in:
            
    Brooks, T. F., & Humphreys, W. M. (2006). A deconvolution
    approach for the mapping of acoustic sources (DAMAS) determined
    from phased microphone arrays. Journal of Sound and Vibration,
    294(4-5), 856-879.
    https://doi.org/10.1016/j.jsv.2005.12.046
        
        
"""

import numpy as np
import scipy.optimize

from .beamformer import BeamformerBase
from .beamformer import vector_prod, calculate_level


class DAMAS(BeamformerBase):
    
    def set_parameters(self):
        self.name = 'DAMAS'
    
    def solve(self,CSM,algo='NNLS',it_max=100,relax=1.0,
              remove_auto=False,verbose=False):
        """ DAMAS deconvolution method
        
        First calculates the convetional beamforming map and later apply
        the deconvolution method. The method consists in solving a linear
        system based on a modelled propagation matrix.
        
        The frequency of the CSM must correspond the one used for calculating
        the steering vectors.
        
        ARGUMENTS
        ----------
        
            CSM: cross-spectral matrix, shape [M,M]
            
            algo: string indicating the algorithm used for solving the
                linear system. Current options are:
                  - `Gauss` - Gauss-Seidel
                  - `NNLS` - non-negative least squares solver (default)
                  
            it_max: maximal number of iterations, only considered in the
                Gauss-Seidel algorithm. Optional, default is 100
            
            relax: relaxation factor, only considered in the
                Gauss-Seidel algorithm. Optional, default is 1.0
            
            remove_auto: boolean indicating if diagonal removal is performed.
                Optional, default is `False`. Preliminary analysis show that
                the Gaussian solver performs better without diagonal removal,
                the opposite forthe NNLS solver.
            
            verbose: print on terminal the convergence of the iterative
                solution, only considered in the Gauss-Seidel algorithm.
                Optional, default is False
        
        RETURNS
        ----------
        
            A: source autopower matrix, shape [N,N]
        
        where M is the total number of microphones in the array and N is the
        total number of scan points in the focal plan.
        
        """
        
        # calculating the propagation matrix
        self.calc_PSF(remove_auto=remove_auto)
        
        # source autopower
        Y = np.zeros((self.number_scan_points,1), dtype=np.complex128)
        # original 'dirty' beamforming map
        for n in range(self.number_scan_points):
            Y[n,] = vector_prod(self.weighted[:,n],CSM)
        
        # 'noise source' at grid point n with levels defined at array, Qn*Qn
        self.A = np.zeros((self.number_scan_points,1), dtype=np.complex128)
        
        if algo == 'Gauss':
            self.A = gauss_seidel(
                y=np.abs(Y),B=self.PSF,it_max=it_max,relax=relax,verbose=verbose)
        elif algo == 'NNLS':
            self.A, rnorm = scipy.optimize.nnls(self.PSF,np.abs(Y[:,0]))
            if verbose: print(f'NNLS residual: {rnorm:12.4e}')
        else:
            raise RuntimeError(f'{algo} solver not implemented')
        
        if verbose: print('\nDone\n')
        
        if remove_auto:
            self.A /= self.number_microphones*(self.number_microphones - 1)
        
        A_real = np.real(self.A)
        self.level = calculate_level(A_real)
        
        return self.A
    
    
def gauss_seidel(y,B,it_max,relax,verbose):
    """ Gauss-Seidel iterative solution to solve y = B x """
    
    x = y.copy().squeeze()*0.0 # initial guess
    N = B.shape[0]
    
    it = 1
    if verbose: print('Iterative solution of the linear problem')
    while it < it_max:
        if verbose and it % 10 == 0:
            # header
            print('{:>4} {:>12} {:>12} {:>12} {:>12}'.format(
                'i','min x','max x','mean x','diff'))
        
        x0 = x.copy()
        for n in range(N):
            solution = y[n] - (
                np.sum(B[n,0:n-1].squeeze()*x[0:n-1]) +
                np.sum(B[n,n+1:].squeeze()*x0[n+1:])
                )
            # if source distribution is negative, set it to zero
            x[n] = (1 - relax)*x[n] + relax*(solution > 0.0)*solution
        
        if verbose:
                print(
            '{:04d} {:12.4e} {:12.4e} {:12.4e} {:12.4e}'.format(
                it,
                *[fun(x) for fun in (np.min, np.max, np.mean)],
                np.abs(np.sum((x - x0)**2)),
                )
            )
        
        it += 1
    
    return x.reshape(y.shape)
