#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""


Base classes and functions defining procedures shared by all the
beamforming solvers


"""

import numpy as np
MACHINE_EPS = np.finfo(np.float64).eps

class BeamformerBase():
    
    def __init__(self,array,grid,frequency,c0=343,r0=None):
        """
        
        ARGUMENTS
        ----------
        
            array: array class instance contaning the coordinates of
                the microphones
                   
            grid: grid class instance containing the coordinates of the
                scan points
            
            frequency: frequency where the analysis is performed
            
            c0: speed of sound in m/s. Optional, default is 343 m/s
            
            r0: distance to the center of the array of microphone in meters.
                See `calc_steering`
        
        RETURNS
        ----------
        
            ##none##
        
        """
        
        self.number_microphones = len(array)
        self.number_scan_points = len(grid)
        
        self.set_parameters()
        
        # calculating the steering vectors
        self.steering, self.weighted = self.calc_steering(
            grid.coordinates,array.coordinates,frequency,c0=c0,r0=r0)
    
    
    def plot(self):
        # to implement
        pass
    
    
    def calc_steering(self,scan_coords,mic_coords,frequency,c0=343,r0=None):
        """ Calculating the free-field steering vector at a given frequency
        
        ARGUMENTS
        ----------
        
            scan_coords: numpy array with coordinates of scan points. N by 3
                array with [x,y,z] coordinates in meters
            
            mic_coords: numpy array with coordinates of microphones. M by 3
                array with [x,y,z] coordinates in meters
            
            frequency: frequency to be analyzed in Hz
            
            c0: sound speed. Optional, default is 343 m/s
            
            r0: distance to the center of the array of microphone in meters.
                Optional, None as default. If given, the steering vector is
                going to be normalized by the amplitude of the sterring
                corresponding the input distance
        
        RETURNS
        ----------
        
            e: steering vectors, [M,N] array, each row corresponds to a
               microphone
              
            w: weighted steering vector
        
        """
        
        N = scan_coords.shape[0]
        M = mic_coords.shape[0]
        
        e = np.zeros( (M,N), dtype=np.complex128 )
        w = e.copy()
        
        if r0 is not None:
            norm_steer = 1/Green_free_field(r0,frequency,c0=c0)
        else:
            norm_steer = 1.0
        
        for n, scan in enumerate(scan_coords):
            for m, microphone in enumerate(mic_coords):
                r = np.sqrt( np.sum((microphone - scan)**2) )
                e[m,n] = Green_free_field(r,frequency,c0=c0)*norm_steer
        
            # weighted steering vector
            # proportional contribution of each steering vector regarding
            # the sum of the “length” of all steering vectors
            
            # considering all microphones for one scan point
            g = np.expand_dims(e[:,n],1)
            gg_vec = g.conj()*g
            gg_array = gg_vec.T*gg_vec
            # ignoring self influence (same microphone)
            np.fill_diagonal(gg_array,0.0)
            
            sum_steering = np.sum(gg_array)
            
            w[:,n] = e[:,n]/np.sqrt(sum_steering)
        
        return e, w
    
    
    def replace_steering(self,new_e):
        """ Replace the steering vector and recalculate weighted vector
        
        ARGUMENTS
        ----------
        
            new_e: steering vectors, [M,N] array, each row corresponds to a
                   microphone
        
        RETURNS
        ----------
        
            ##none##
        
        """
        w = new_e.copy()
        
        for n in range(new_e.shape[1]):
            # considering all microphones for one scan point
            g = np.expand_dims(new_e[:,n],1)
            gg_vec = g.conj()*g
            gg_array = gg_vec.T*gg_vec
            # ignoring self influence (same microphone)
            np.fill_diagonal(gg_array,0.0)
            
            sum_steering = np.sum(gg_array)
            
            w[:,n] = new_e[:,n]/np.sqrt(sum_steering)
        
        self.steering = new_e
        self.weighted = w
    
    
    def calc_PSF(self,remove_auto=True):
        """ Calculating the propagation matrix
        
        The propagation matrix has the reciprocal influence of beamforming
        characteristics between grid points. Each column is the
        point-spread function (PSF) at each of the grid points.
        
        As defined in:
            
            Brooks, T. F., & Humphreys, W. M. (2006). A deconvolution
            approach for the mapping of acoustic sources (DAMAS) determined
            from phased microphone arrays. Journal of Sound and Vibration,
            294(4-5), 856-879.
            https://doi.org/10.1016/j.jsv.2005.12.046
        
        
        Must be called after the steering vectors have been calculated
        
        ARGUMENTS
        ----------
        
            remove_auto: boolean indicating the use of diagonal removal
        
        RETURNS
        ----------
        
            A: the propagation matrix, format [N,N]
        
        """
        
        if not hasattr(self,'steering'):
            raise 'Steering vector must be available'
        
        A = np.zeros((self.number_scan_points,self.number_scan_points,),
                     dtype=np.complex128)
        for n_ in range(self.number_scan_points):
            inv_steer = 1/np.expand_dims(self.steering[:,n_], 1)
            bracketed_term = inv_steer.conj()*inv_steer.T
            if remove_auto: np.fill_diagonal(bracketed_term,0.0)
            for n in range(self.number_scan_points):
                A[n,n_] = vector_prod(self.steering[:,n],bracketed_term)
        
        if remove_auto:
            A /= self.number_microphones**2 -self.number_microphones
        
        A = np.real(A) # ignoring complex part - null
        
        self.PSF = A
        
        return A


def vector_prod(x,A):
    """ Function to calcalte x* dot A dot x """
    return np.matmul(np.matmul(x.conj().T, A),x)


def calculate_level(A):
    """ Function to relative level in decibels
    
    Replace null and negatives values by a small number
    to remove ilegal mathematical operations
    
    """
    A[A <= 0] = MACHINE_EPS
    return 10*np.log10(A/np.nanmax(A))


def Green_free_field(r,f,c0=343):
    """ Free field Green function for a monopole, isotropic propagation
    
    ARGUMENTS
    ----------
    
        r: distance from source to microphone
        f: frequency of source in Hz
        c0: sound speed. Optional, default is 343 m/s
        
    RETURNS
    ----------
    
        Green function result
    
    """
    # angular frequency
    omega = 2*np.pi*f
    # wave number
    k = omega/c0
    
    return np.exp(-1j*k*r)/(4*np.pi*r)